FROM debian:12.7-slim

ENV USER=liar
ENV HOME=/home/$USER
ENV HOME_CONFIG=$HOME/.config
ENV CONFIG=$HOME_CONFIG/liar

LABEL description="liar docker image"
LABEL date="2024-10-12"

RUN apt-get --quiet update \
    && apt-get --no-install-recommends --assume-yes install \
        aria2 \
        bash-completion \
        bzip2 \
        ca-certificates \
        coreutils \
        curl \
        debianutils \
        diffutils \
        findutils \
        git \
        gzip \
        less \
        mawk \
        ncompress \
        shunit2 \
        sudo \
        tar \
        unzip \
        vim \
        wget \
        xdg-utils \
        xz-utils \
    && rm --recursive --force /var/lib/apt/lists/* \
    && useradd --groups sudo --home-dir $HOME --create-home --shell /bin/bash $USER \
    && sed --in-place "s/ALL$/NOPASSWD: ALL/" /etc/sudoers

RUN sudo --user=$USER mkdir --parents $HOME/Programs $HOME/bin \
    && echo '[ -d "$HOME/bin" ] && PATH="$HOME/bin:$PATH"' | tee --append $HOME/.bashrc \
    && wget --no-verbose --output-document=- https://github.com/cheat/cheat/releases/download/4.4.2/cheat-linux-amd64.gz | gzip --decompress > /usr/local/bin/cheat \
    && chmod 755 /usr/local/bin/cheat

COPY --chmod=755 liar liar-test /usr/local/bin/
COPY liar-completion /etc/bash_completion.d/
COPY --chown=$USER config/* $CONFIG/

USER $USER
WORKDIR $HOME
