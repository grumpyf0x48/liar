# Linux Installer for Archives

## Table of contents

- [Features](#features)
- [Installation](#installation)
- [Recent changes](#recent-changes)
- [Sample use](#sample-use)
- [Documentation](#documentation)
- [Dependencies](#dependencies)
- [Tested software](#tested-software)
- [Unit tests](#unit-tests)
- [Docker image](#docker-image)
- [Try it](#try-it)
- [Similar software](#similar-software)
- [FAQ](#faq)
- [License](#license)

**L**inux **I**nstaller for **AR**chives a.k.a. `liar` is an easy to use command
to install software packaged as an archive on Linux systems.

Nowadays more and more software and tools are delivered as a tarball or
compressed one instead of being installed from Linux official repositories. A
typical example of this is [JDK](http://jdk.java.net/). It also happens that a
software present in the Linux repository is not up to date enough for your
needs.

Moreover, there is an increasing need to not only being able to install software
without having administrator privileges but also to have the ability to install
several versions of the same software on the same machine.

That's why `liar` addresses simply these needs with a standalone Shell script to
install, list and remove software packaged in an archive file of one of the
following
formats: **.tar** **.tar.bz2** **.tar.gz** **.gz** **.tar.xz** **.tar.Z** **.tbz2**
**.tgz** and **.zip**. Single binary files are also supported.

Very few dependencies are needed, see [Dependencies](#dependencies) section for
details.

It is developed using [Ubuntu MATE](https://ubuntu-mate.org) version 20.04.3
using **dash** version 0.5.10.2-6. Additionally, tests are done on
[Debian GNU/Linux](https://www.debian.org) version 10.7 with dash version
0.5.10.2-5 and conformance to **sh** syntax is checked with
[ShellCheck](https://www.shellcheck.net) and
[checkbashisms](https://sourceforge.net/projects/checkbaskisms/) tools.

## Features

`liar` provides installation, listing and removal of tarball packaged software
in a simple command with:

- Installation of software archives located on filesystem or network
- Destination folder selection
- Possibility to download and cache an archive before its installation
- Internal software archives cache, optional for local archives
- Download user agent choice (wget, curl or aria2c) for network archives
- Download resume (network archives)
- Secure installation to prevent software duplicates in a given destination
  folder
- Integrity for non `liar` installed software or unknown folder as `liar` does
  not remove nor list software it has not installed itself
- Secure software removal guaranteed by interactive user confirmation
- Optional root usage
- Symbolic links creation for installed binaries with folder selection and
  binaries filtering
- Bash command line completion to help entry of options, commands and
  parameters
- Possibility to define default options and parameters for all commnds
- Define https_proxy and no_proxy for download command
- Execution from file command with shebang

For a full list of features, see also [New features](#new-features) section
which describes what's new since version 0.1.

`liar` has been tested with **more than 60 software** including Atom, GitHub
CLI, Go, Gradle, IntelliJ IDEA and other JetBrains IDEs, JBang, JDK
(Oracle build under GPL license, Adoptium, Zulu OpenJDK), JDK Mission
Control (JMC), JReleaser, Maven, Robo 3T, Spring Boot CLI, Sublime Merge,
Sublime Text, Visual Studio Code.

For a complete list of tarball delivered software tested with `liar`, see
[Tested Software](#tested-software).

Of course `liar` is also able to install software not listed here, as all it
expects is to have the software delivered as an archive.

## Installation

View the most commonly used commands in the [Getting started with Linux
Installer for Archives](https://asciinema.org/a/109135) video:

<a href="https://asciinema.org/a/109135?autoplay=1" title="Getting started with
Linux Installer for Archives" target="_blank"><img src="GettingStarted.png"
width="50%" height="50%" /></a>

`liar` [version 0.1](https://framagit.org/grumpyf0x48/liar/tree/0.1) has been
released on Fri, 14 Apr 2017.

To install the released version, jump to
[Released version installation](#released-version-installation).

Otherwise, go on with the development version.

### Development version installation

`liar` current development version is
[version 0.2-dev](https://framagit.org/grumpyf0x48/liar/tree/master).

#### Basic installation

```console
sudo \
      curl \
          -L https://git.framasoft.org/grumpyf0x48/liar/raw/master/liar \
          -o /usr/local/bin/liar
```

or you can use `wget` instead:

```console
sudo \
      wget \
          https://git.framasoft.org/grumpyf0x48/liar/raw/master/liar \
          -O /usr/local/bin/liar
```

Then:

```console
sudo chmod 755 /usr/local/bin/liar
```

#### Bash completion installation

`liar` supports Bash command line completion for options, commands and
parameters. Default values for options with parameters is also handled.

To use it, enter the following command:

```console
sudo \
      curl \
          -L https://git.framasoft.org/grumpyf0x48/liar/raw/master/liar-completion \
          -o /etc/bash_completion.d/liar-completion
```

Alternatively you can also use `wget`:

```console
sudo \
      wget \
          https://git.framasoft.org/grumpyf0x48/liar/raw/master/liar-completion \
          -O /etc/bash_completion.d/liar-completion
```

Then, start a new Bash interpreter.

#### Cheatsheet installation

`liar` now ships with a [cheatsheet](config/liar-cheat) usable with
[cheat](https://github.com/cheat/cheat) command.

To install `liar` cheatsheet in your personal cheatsheets folder, after
[having installed](https://grumpyf0x48.org/?p=2247) `cheat`:

```console
curl \
      -L https://git.framasoft.org/grumpyf0x48/liar/raw/master/cheat/liar \
      -o ${HOME}/.config/cheat/cheatsheets/personal/liar
```

or with `wget`:

```console
wget \
      https://git.framasoft.org/grumpyf0x48/liar/raw/master/cheat/liar \
      -O ${HOME}/.config/cheat/cheatsheets/personal/liar
```

You can now use the cheatsheet with:

```console
cheat liar
```

#### New features

Since `liar` version 0.1, the following features and changes have been added to
the current version.

- Software archive checksum verification (md5, sha1, sha256 or sha512)
- Improvements for binaries filtering in symbolic links creation
- Simplified usage with samples only displayed in verbose mode
- Automatic completion of software names and urls for more than 60 tested
  software (for x86_64 architecture)
- Remove command is now able to remove several software at a time
- Complete rewrite of command line completion
- list command output can be sorted by any column
- Ability to upgrade `liar` and `liar-completion` files (development version
  only)
- Manual and periodic upgrade of [liar-software](#liar-software) file from the
  [Git repository](https://framagit.org/grumpyf0x48/liar)
- Several patterns can now be used for symbolic links creation
- Possibility to download and cache an archive before its installation
- New command to display cache list and cached file details
- list command accepts an optional parameter which is a list of software
- Installed software version has been added to the list command
- New command to clean an archive from the cache
- Allow executing commands from [standard input](#standard-input-or-file-command-execution) or from a file
- Add [cheatsheet](config/liar-cheat) for `liar` command
- [Configuration files](#configuration-files) stored according to the XDG Base
  Directory specification
- New command [config](#config) to display `liar` configuration files
- Harmonized cache commands parameters with `install` command
- New Command [cheat](#cheat) to display `liar` cheatsheet
- Possibility to define default options and parameters for all commnds
- Define https_proxy and no_proxy for download command
- Execution from file command with [shebang](#file-command-execution-with-shebang)
- Allow separate [upgrade](#upgrade) of `liar` configuration or binary files
- [version](#version) command now displays `liar` binary files in verbose mode.
  It was previously done by the [config](#config) command.
- Add new `-L` flag to follow redirects and use [Location header](#redirections-and-location-header)
- [liar-mime-types](#liar-mime-types) file can now be customised

### Released version installation

#### Basic installation

To install `liar` 0.1 version, simply enter:

```console
sudo \
      curl \
          -L https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar \
          -o /usr/local/bin/liar
```

Or you can use `wget` instead:

```console
sudo \
      wget \
          https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar \
          -O /usr/local/bin/liar
```

Then:

```console
sudo chmod 755 /usr/local/bin/liar
```

#### Installation without sysadmin rights

Without sysadmin rights, use the following command:

```console
curl \
      -L https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar \
      -o ~/bin/liar
```

Or:

```console
wget \
      https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar \
      -O ~/bin/liar
```

Then:

```console
sudo chmod 755 ~/bin/liar
```

#### Bash completion installation

To install Bash command line completion, enter the following command:

```console
sudo \
      curl \
          -L https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar-completion \
          -o /etc/bash_completion.d/liar-completion
```

Alternatively you can also use `wget`:

```console
sudo \
      wget \
          https://git.framasoft.org/grumpyf0x48/liar/raw/0.1/liar-completion \
          -O /etc/bash_completion.d/liar-completion
```

Then, start a new Bash interpreter.

## Recent changes

### New software handled in 2022

- DbVisualizer
- Gitless
- lazygit
- fzf
- umlet

### New software handled in 2021

- GraalVM in versions 8, 11 and 16
- Adoptium JDK (Initially AdoptOpenJDK)
- JReleaser
- Sublime Text 4
- dbeaver
- delta
- JMC
- act
- OVH venom

### Zulu builds of OpenJDK by Azul

`liar-software` now includes URLs for Zulu OpenJDKs in versions 11 to 21.

### Redirections

`wget` and `curl` user agents can now be used with `-L` flag to follow
[redirections](#redirections-and-location-header).

### upgrade command improvements

`upgrade` command now allows to [upgrade separately](#upgrade) `liar`
configuration or binary files.

### Adoptium prebuilt JDK binaries

`liar-software` now includes URLs for Adoptium in versions 11, 16, 17, 18, 19 and 20.

To install Adoptium with `liar` using Bash completion, use the `-e` flag.

### Execution from file command with shebang

You can now run command files directly from a
[file with a shebang](#file-command-execution-with-shebang).

### 25 handled software added in 2020

`liar` can now install the following software:

- bat
- cheat
- clion
- cloc
- datagrip
- easy_rsa
- exa
- gh
- glab
- grpcui
- hadolint
- helidon
- idea_edu
- JBang
- jd_cmd
- jgo
- jq
- micronaut
- mongodb
- pycharm_edu
- shellcheck
- vscode
- vscodium
- youtube_dl
- yq

Plus the following JDK:

- jdk14
- jdk15
- jdk16
- jdk_ea17

### Use proxy for download command

`download` command can now be done through [a proxy](#define-proxy-host).

It is also possible to define hosts for which the use of the proxy will be
disabled.

### Define default options and parameters for all commnds

Possibility to set
[default options and parameters](#use-default-options-and-parameters) for all
commands.

### cheat command

A new command `cheat` has been added to ease the use of the cheatsheet:

```console
liar cheat
```

### Try liar with Repl.it

You can now try `liar` in your browser with
[Repl.it](https://repl.it/@grumpyf0x48/liar).

### Cache commands parameters changes

Cache commands `display` and `clean` now accept an optional NAME parameter to be
coherent with `install` and `download` commands and ease command line
completion.

Also, this parameter is now optional for `download` command.

See [cache commands](#download) documentation for more details.

### XDG Base Directory

[Configuration and cache files](#configuration-files) now follow the XDG Base
Directory specification.

Moreover, some configuration files have been renamed.

As a consequence, the following files and folders:

- `$HOME/.liar_software`
- `$HOME/.liar_mime_types`
- `$HOME/.liar_cache`

are now located (when `XDG_` environment variables are set) in:

- `$XDG_CONFIG_HOME/liar/liar-software`
- `$XDG_CONFIG_HOME/liar/liar-mime-types`
- `$XDG_DATA_HOME/liar/cache`

otherwise, they are located in:

- `$HOME/.config/liar/liar-software`
- `$HOME/.config/liar/liar-mime-types`
- `$HOME/.local/share/liar/cache`

**Warning:** No migration is performed to move nor rename any file or folder.

### Standard input command execution

`liar` can now read commands from standard input of from a file.

See Standard input or
[file command execution](#standard-input-or-file-command-execution).

## Sample use

`liar` sample use has been moved to [SAMPLES](SAMPLES.md) file.

## Documentation

This section describes which commands,
[options and parameters](#options-and-parameters) `liar` accepts.

### Commands

`liar` supports the following commands:

- help
- install
- list
- remove
- upgrade
- config
- version
- cheat

used to:

- Getting help
- Install a software
- List installed software
- Remove one or several software
- Upgrade `liar`
- Display `liar` configuration
- Print `liar` version
- Display `liar` cheatsheet

Additionally, the following commands act on the local archive cache:

- download
- display
- clean

to respectively:

- Download a software
- Display the cached archives or the content of a cached archive
- Clean an archive

It is also possible to read and execute commands
[from standard input](#standard-input-or-file-command-execution) or from a file.

Finally, [default options and parameters](#use-default-options-and-parameters)
can be set for all commands.

#### help

To get help of `liar` usage, simply enter:

```console
liar help
Usage: liar [OPTIONS...] COMMAND [PARAMETERS...]

      Handles installation, removal and listing of software packaged in an archive.

      Supported archive types are: .tar .tar.bz2 .tar.gz .gz .tar.xz .tar.Z .tbz2 .tgz and .zip.
      Single binary files are also supported.

      OPTIONS:
                -d      Specify DESTINATION folder. If unspecified, installation will be done in /home/user/Programs.
                -l      Create symbolic links in /home/user/bin folder after installation.
                -h      Remove extension for SHELL symbolic links
                -s      Specify the folder for SYMBOLIC links creation.
                -p      Specify the file PATTERN(s) to restrict symbolic links creation.
                -n      Use -name find flag instead of default -wholename flag for executables search.
                -u      Specify the download user AGENT: wget, curl or aria2c. By default, wget is used.
                -e      Use content disposition header.
                -L      Use location header.
                -k      Bypass server certificate check.
                -y      Use PROXY for downloads.
                -o      Define excluded HOSTS when using proxy.
                -c      Specify the file path expected CHECKSUM.
                -t      Specify the checksum TYPE: md5, sha1, sha256 or sha512. By default sha1 is used.
                -S      Specify the sort COLUMN to use in list command.
                -v      Activate VERBOSE mode.
                -x      Activate VERBOSE mode for download operations.
                -r      Allow ROOT usage.
                -b      Bypass CACHE use for local archives.
                -f      Bypass user CONFIRMATION.

       COMMANDS:
                 install NAME FILE_PATH           Install a software packaged in the archive FILE_PATH into the folder DESTINATION under the name NAME.
                 list [NAMES...]                  List installed software.
                 remove NAMES...                  Remove the software(s) named NAMES.
                 upgrade [config | binaries]      Upgrade $LIAR configuration, binaries or both.
                 config                           Display liar configuration.
                 version                          Print the version of liar.
                 cheat                            Display liar cheat.
                 help                             Display this help.

       CACHE COMMANDS:
                 download [NAME] FILE_PATH        Download or cache the software from the archive FILE_PATH.
                 display [NAME] [FILE_PATH]       Display the cached archives or the content of the cached archive FILE_PATH.
                 clean [NAME] FILE_PATH           Clean the cached archive FILE_PATH.
```

With `-v` option, `liar` help output also includes sample commands:

```console
liar -v help

...
      EXAMPLES:
                liar install sublime https://download.sublimetext.com/sublime_text_3_build_3211_x64.tar.bz2
                liar -l install jbang https://github.com/jbangdev/jbang/releases/download/v0.52.0/jbang-0.52.0.zip
                liar -l -p "sublime*" install sublime Downloads/sublime_text_3_build_3126_x64.tar.bz2
                liar -v -a curl install idea_community https://download.jetbrains.com/idea/ideaIC-2020.2.3.tar.gz
                liar remove sublime
                liar download sublimemerge https://download.sublimetext.com/sublime_merge_build_2025_x64.tar.xz
                liar display maven apache-maven-3.3.9-bin.tar.gz
                liar -S 5 list
                liar -r -d /opt/liar-installations list
```

#### install

In its simplest form, the install command takes only two parameters.

For example:

```console
liar install jdk8 ~/Downloads/jdk-8u101-linux-x64.tar.gz
Installing 'jdk8' from '/home/user/Downloads/jdk-8u101-linux-x64.tar.gz' to '/home/user/Programs/jdk8'
Extracting '/home/user/.liar_cache/jdk-8u101-linux-x64.tar.gz' to '/home/user/Programs/jdk8'
```

The parameters are:

- the name to give to the software being installed
- the input archive containing the software

Additionally, all `liar` options except `-f` can be used to alter the install
command behavior.

See [Options and parameters](#options-and-parameters) section for more details.

#### download

The download command takes the same parameters as the install one but only
downloads the archive to the cache without installing it.

For example:

```console
liar \
      download \
      sublimemerge \
      https://download.sublimetext.com/sublime_merge_build_1119_x64.tar.xz
```

Once the archive has been downloaded it can be installed later on with slightly
the same command line:

```console
liar \
      install \
      sublimemerge \
      https://download.sublimetext.com/sublime_merge_build_1119_x64.tar.xz
```

Having replaced `download` by `install`.

The cached archive will then be used during the installation.

If the last argument to the download command is a local file, then this file
will be cached.

**Note:** The optional software name parameter of the `download`, `display` and
  `clean` commands is only here to ease bash completion of the software archive
  path or URL.

#### display

The display content can display either all cached archives:

```console
liar display
```

In this case, all archives found in `$XDG_DATA_HOME/liar/cache` folder are
listed.

or the content of a given archive:

```console
liar display micronaut-1.1.0.zip
```

which lists the content of the chosen archive.

Another possible syntax is:

```console
liar display micronaut micronaut-1.1.0.zip
```

#### clean

The clean command deletes an archive from the cache.

For example:

```console
liar clean openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz
```

Other syntax:

```console
liar clean jdk8 openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz
```

#### list

The list command displays which software are installed in the DESTINATION
folder:

```console
liar list
Listing installed software in '/home/user/Programs'

Name           Version    Folder             File Name                              Date

spring         2.2.2      spring-2.2.2.REL   spring-boot-cli-2.2.2.RELEASE-bin.zi   2019/12/28
maven          3.6.3      apache-maven-3.6   apache-maven-3.6.3-bin.tar.gz          2019/12/28
sublimemerge   1107       sublime_merge      sublime_merge_build_1107_x64.tar.xz    2019/04/27
torbrowser     7.5.4      tor-browser_fr     tor-browser-linux64-7.5.4_fr.tar.xz    2018/05/26
```

It is also possible to display only some installed software:

```console
liar list gradle graalvm micronaut
Listing installed software in '/home/user/Programs'

Name           Version    Folder             File Name                              Date

gradle         6.3        gradle-6.3         gradle-6.3-bin.zip                     2020/04/13
graalvm        19.2.1     graalvm-ce-19.2.   graalvm-ce-linux-amd64-19.2.1.tar.gz   2019/10/19
micronaut      1.1.0      micronaut-1.1.0    micronaut-1.1.0.zip                    2019/04/21
```

When using verbose mode, more info is displayed:

```console
liar -v list
Enabling verbose mode
DESTINATION option is not set, Using '/home/user/Programs' by default
Listing installed software in '/home/user/Programs'

Name           Version    Folder             File Name                              Sha1       Date         Time       Links

spring         2.2.2      spring-2.2.2.REL   spring-boot-cli-2.2.2.RELEASE-bin.zi   5f044c23   2019/12/28   10:19:06   1
maven          3.6.3      apache-maven-3.6   apache-maven-3.6.3-bin.tar.gz          cc836dc7   2019/12/28   10:54:51   3
sublimemerge   1107       sublime_merge      sublime_merge_build_1107_x64.tar.xz    1de285d2   2019/04/27   11:39:15   1
torbrowser     7.5.4      tor-browser_fr     tor-browser-linux64-7.5.4_fr.tar.xz    fdb76498   2018/05/26   05:47:58   1
```

By default, the list command output is ordered by last installation date.

To sort the output using another column, `-S` option may be used. For example,
to sort according to the name:

```console
liar -S1 list
Setting sort column to: 1
Setting sort order to: 'normal'
Listing installed software in '/home/user/Programs'

Name           Version    Folder             File Name                              Date

firefox        64.0.2     firefox            firefox-64.0.2.tar.bz2                 2019/01/27
graalvm_rc16   1.0.0.16   graalvm-ce-1.0.0   graalvm-ce-1.0.0-rc16-linux-amd64.ta   2019/10/20
hugo           0.31.1     hugo               hugo_0.31.1_Linux-64bit.tar.gz         2017/12/09
idea_communi   2018.2     idea-IC-182.3684   ideaIC-2018.2.tar.gz                   2018/08/06
marp           0.0.13     locales            0.0.13-Marp-linux-x64.tar.gz           2018/07/28
maven          3.6.3      apache-maven-3.6   apache-maven-3.6.3-bin.tar.gz          2019/12/28
spring         2.2.2      spring-2.2.2.REL   spring-boot-cli-2.2.2.RELEASE-bin.zi   2019/12/28
sublimemerge   1107       sublime_merge      sublime_merge_build_1107_x64.tar.xz    2019/04/27
torbrowser     7.5.4      tor-browser_fr     tor-browser-linux64-7.5.4_fr.tar.xz    2018/05/26
```

A negative value supplied to `-S` option will reverse the sort.

#### remove

The remove command takes one or more parameters, which are the name(s) of the
software(s) to remove. The software name was given by the first parameter of
the install command.

A typical remove sample use is:

```console
liar remove maven3
Removing 'maven3' installed in '/home/user/Programs/maven3'
Do you want to remove 'maven3' installed in '/home/user/Programs/maven3' (Y/N) ? Y
```

When removing a software, a confirmation is asked to the user. This confirmation
can be removed if `-f` option is used:

```console
liar -f remove maven3
Enabling force mode
Removing 'maven3' installed in '/home/user/Programs/maven3'
```

The remove command can be used with the general options described below.

#### upgrade

The upgrade command allows to upgrade binary files:

- `liar` and `liar-completion` files with the latest development version
  available (development version only)

and configuration files:

- `liar-software` file (when not customized)
- `liar-mime-types` file
- `liar-cheat` cheatsheet to be displayed with cheat command

When no parameter is provided, the command upgrades liar binary and
configuration files.

If you want to upgrade only `liar` binary files, use:

```console
sudo liar -r upgrade binaries
```

To upgrade `liar` configuration files:

```console
sudo liar -r upgrade config
```

The upgrade command prompts the user for confirmation. If you want to bypass
confirmation, simply add `-f` flag to the previous commands.

**Note:**

If you use one of `LIAR_SOFTWARE_FILE` or `LIAR_MIME_TYPES_FILE` environment variables, you will need to use:

```console
sudo --preserve-env liar -r upgrade ...
```

##### non-root installation

We have just seen commands for root installations.

For non-root installations, use:

```console
liar upgrade
```

#### config

`config` command displays `liar` configuration files and cache folder location:

```console
liar config
Listing 'liar' configuration files

File Path                                  Sha1       Date         Time

/home/user/.config/liar/liar-software      b639ecef   2021/04/08   06:52:53
/home/user/.config/liar/liar-mime-types    498f5b20   2021/04/05   09:31:51
/home/user/.config/liar/liar-cheat         8348692b   2021/04/06   18:12:43
/home/user/.local/share/liar/cache                    2021/03/31   07:50:26
```

#### version

To display the version of `liar` being used, enter:

```console
liar version
liar version 0.1
```

In verbose mode `liar` also displays binary files:

```console
liar -v version
Enabling verbose mode
liar version 0.2-dev (https://framagit.org/grumpyf0x48/liar/raw/master)
Listing 'liar' binary files

File Path                                        Sha1       Date         Time

/usr/local/bin/liar                              7e7d5a3f   2021/04/08   07:49:43
/etc/bash_completion.d/liar-completion           606d3938   2021/04/06   18:12:42
```

#### cheat

If you have `cheat` installed, you can easily display `liar` cheatsheet with:

```console
liar cheat
# install GitHub CLI with symbolic link created
liar -l install gh https://github.com/cli/cli/releases/download/v1.0.0/gh_1.0.0_linux_amd64.tar.gz
...
```

`liar` will then download the latest cheatsheet and call the `cheat` command as
if you typed:

```console
cheat liar
```

You can also use this command for a specific software:

```console
cheat liar spring
```

#### Standard input or file command execution

`liar` commands described above can now be executed from standard input.

Typical uses are:

```console
liar < cmd_file
```

```console
cat cmd_file | liar
```

or:

```console
liar cmd_file
```

In this last example, only one parameter (the command file) should be provided.

Having a command file like this:

```console
cat cmd_file
version
help
install sublime Downloads/sublime_text_3_build_3126_x64.tar.bz2
list
```

You can also use here strings:

```console
liar < <(printf "version\nhelp\n-v list\n")
```

Or:

```console
printf "version\nhelp\n-v list\n" | liar
```

The execution of commands will stop after the first failing command.

**Note:** When using command execution from standard input or from a file, any
  option passed on the command line will be ignored. If you need to set default
  options for all commands, read
  [Use default options and parameters](#use-default-options-and-parameters).

#### File command execution with shebang

It is also possible to execute commands from a command file like this:

```console
cat cmd_file
//usr/bin/env liar "$0" "$@" ; exit $?
list
version
```

And then run:

```console
chmod u+x cmd_file
./cmd_file
Reading commands from: ./cmd_file
WARNING: command line options will be ignored

Executing command: 'liar list':
...
```

This feature was inspired by [JBang](https://jbang.dev).

#### Use default options and parameters

It is possible to set default options and parameters for all commands.

Examples:

If you want all `liar` commands to be executed in verbose mode, add the
following code to your `.bashrc` file:

```console
export LIAR_DEFAULT_ARGS="-v"
```

If you want to bypass server certificate checks and use `aria2c` user agent:

```console
export LIAR_DEFAULT_ARGS="-k -u aria2c"
```

In the case where an option or parameter is set by default and on the command
line, the command line one will take precedence.

### Options and parameters

Now that we have seen which commands `liar` implements, let's see the options
they can use, and the parameters they need.

General options are:

- DESTINATION folder (`-d`)
- VERBOSE mode (`-v`)
- VERBOSE mode for download operations (`-x`)
- ROOT usage (`-r`)
- Bypass CACHE (`-b`)
- Bypass user CONFIRMATION (`-f`)
- Download user AGENT (`-u`)
- Content disposition header (`-e`)
- Location header (`-L`)
- Bypass server certificate check (`-k`)
- Define PROXY host (`-y`) and excluded HOSTS (`-o`) for download command
- CHECKSUM type and value (`-t`, `-c`)

Symbolic links creation options are:

- Remove extension for SHELL symbolic links (`-h`)
- folder for SYMBOLIC links creation (`-s`)
- file PATTERN(s) for SYMBOLIC links (`-p`)
- find TYPE for executables search (`-n`)

Command parameters are:

- software NAME
- archive FILE_PATH

#### Destination folder

`liar` helps you to install remove and list software that is packaged as a
tarball, the FILE_PATH, in a folder of your choice, the DESTINATION folder.

DESTINATION folder default value is: `$HOME/Programs`.

This option can be used with install, list and remove commands.

#### Name

When installing a software `liar` needs two mandatory parameters, the NAME and
the FILE_PATH.

NAME is used to create a sub-folder in the DESTINATION folder in order to
install the desired software.

The NAME parameter is also mandatory when removing a software. Several NAME
parameters may also be supplied to the remove command when removing several
software at a time.

#### File path

FILE_PATH is the path of the archive containing the software to install.

The FILE_PATH parameter is specific to the install command.

#### Verbose mode

When option `-v` is set, verbose mode is activated. In this mode, some commands
display more detailed output when executed.

#### Verbose mode for download operations

When option `-x` is set, more verbose output is displayed by download operations
done with `wget`, `curl` or `aria2c`.

#### Root usage

By default, `liar` will not let you install, list nor remove software when
logged as root. To allow root usage `-r` option should be used.

#### Bypass cache

`liar` uses an internal cache for the archives it installs. It is used for
example for download resume. This cache may be disabled when installing a local
archive using `-b` option.

#### Bypass user confirmation

When removing one or several software, or when using the command upgrade, a
confirmation is asked to the user to prevent unwanted removal. To skip this
confirmation, `-f` option may be used.

#### Download user agent

When installing an archive that is downloaded from the internet, the default
user agent is `wget`.

It is possible to change this behavior using `-u` flag with one of the following
values:

- aria2c
- curl
- wget

#### Content disposition header

Some software providers supply their URL in the form of
https://dl.pstmn.io/download/latest/linux64 making use of the content
disposition header. In this case, the `-e` flag should be used to get the
effective file name.

#### Redirections and Location header

Some servers redirect the request to another URL, making the use of the Location
header useful. In this case, the `-L` flag should be used to get the real file
name.

**Warning:** `-e` and `-L` flags are mutually exclusive and are not implemented
  with `aria2c`.

#### Bypass server certificate check

`-k` option allows downloading software from https servers that are not trusted
locally.

#### Define proxy host

`-y` option allows to define a PROXY to connect to when downloading software.

It takes one parameter with the following syntax: host:port.

For example:

```console
liar \
      -y localhost:3128 \
      download \
      jbang \
      https://github.com/jbangdev/jbang/releases/download/v0.57.0/jbang-0.57.0.zip
```

**Note:** `liar` does not uses proxy environment variables HTTPS_PROXY and
  NO_PROXY, So no proxy will be used unless `-y` flag is used.

#### Define excluded hosts when using proxy

`-o` option takes one parameter to define hosts for which not to use the proxy.

This parameter is a comma separated list of hosts or domain extensions.

#### Checksum type and value

`liar` supports checksum verification for downloaded file path.

To set the checksum of the file to install, use the `-c` option followed by the
SHA1 checksum. If the checksum is not a SHA1 but a MD5, SHA256 or SHA512, you
can set the checksum type using the `-t` flag.

For example:

```console
liar \
      -t sha256 \
      -c a5a4f948d259734ac5649122d0c2f7e885b48162cde78e43415638cbfd7a7aa3 \
      install \
      idea_community \
      https://download-cf.jetbrains.com/idea/ideaIC-2018.1.1.tar.gz
```

#### Symbolic links creation

After a software is installed `liar` can optionally create symbolic links for
installed executables to a folder of your choice, the folder for SYMBOLIC links
creation.

Symlinking can be done for all or only some executables installed.

In this case, the file PATTERN(s) used to filter the executables to symlink is
(are) a(some) pattern(s) understood by the **find** command **-wholename**
flag (by default).

If `-n` flag is used, the **find** command **-name** flag is used instead.

Several patterns should be separated by `|`.

For convenience, an initial `*` is added to the beginning of each pattern if not
already present.

The folder for SYMBOLIC links creation is optional and has the following default
values:

- `/usr/local/bin` for root installation
- `$HOME/bin` otherwise

Additionally, `-h` flag now allows removing `.sh` extension for symbolic links
pointing towards SHELL files:

```console
ls -l bin/jgo
lrwxrwxrwx 1 user user 46 sept. 10 07:09 bin/jgo -> /home/user/Programs/jgo/jgo-0.3.0/jgo.sh
```

### Configuration files

`liar` uses configuration files located in:

- `$XDG_CONFIG_HOME/liar` if `XDG_CONFIG_HOME` is set
- `$HOME/.config/liar` otherwise

These files are created at `liar` first use.

The user can modify these files, but he should keep in mind that these
modifications may be lost if the configuration files are updated by a new
version of `liar`.

#### liar-mime-types

When creating [symbolic links](#symbolic-links-creation), this file is used to
determine whether a file is an executable or not.

`liar-mime-types` looks like:

``` application/x-executable application/x-perl application/x-sh
application/x-shellscript application/x-tcl text/x-perl text/x-python text/x-sh
```

It is created in `liar` config folder when links creation of `install` command
needs it.

It is located in the `config` folder of the Git repository.

`liar-mime-types` can be customised by the user as follows:

```console
export LIAR_MIME_TYPES_FILE=custom
```

#### liar-software

This file is a catalog of software tested with `liar` and defines their version
and archive URL.

It is used by [command line completion](#bash-completion-installation).

It is upgraded at `bash` startup when it is outdated and can also be upgraded
manually with the [upgrade](#upgrade) command.

`liar-software` can be customised by the user for example to add some
software.

In this case, upgrade of the file should be disabled as follows:

```console
export LIAR_SOFTWARE_FILE=custom
```

## Dependencies

`liar` requires nothing more than a Bourne Shell i.e. **sh** or compatible one
with the following packages installed:

- **coreutils**
- **debianutils**
- **diffutils**
- **findutils**
- **mawk**
- **tar**

Additionally, the following packages providing compression programs are required
depending of the archive format used:

- **bzip2**: .tar.bz2 .tbz2
- **gzip**: .tar.gz .gz .tgz
- **ncompress**: .tar.Z
- **unzip**: .zip
- **xz-utils**: .tar.xz

Moreover, **aria2**, **curl** or **wget** package is needed when using archive
download feature.

If installed, **xdg-utils** package will be used to filter executables when
creating links.

**bash-completion** is required when using Bash command line completion.

Finally, [cheat](https://github.com/cheat/cheat) needs to be installed if you
want to use `liar` [cheat](#cheat) command.

## Tested software

`liar` has been tested with the following software:

- act
- Adoptium
- Apache Ant
- Apache JMeter
- Apache Maven
- Atom
- bat
- Cheat
- cloc
- Cloud Foundry CLI
- confluent CLI
- DBeaver
- DbVisualizer
- delta
- easy-rsa
- Evans
- exa
- fzf
- GitHub CLI
- glab
- Gitless
- Go
- GraalVM 17 & 20
- Gradle
- Grails
- Groovy
- gRPC UI
- gRPCurl
- hadolint
- Helidon
- Heroku
- Hugo
- Icecat
- jbang
- jd-cmd
- JDK Early Access and General Availability Releases (Oracle build under GPL
  license)
- JDK Mission Control (JMC)
- JetBrains IDE (CLion, DataGrip, GoLand, IntelliJ IDEA, PyCharm, RubyMine,
  WebStorm, ...)
- jgo
- jmeter
- jq
- JReleaser
- kotlin
- lazygit
- Marp CLI
- Maven Central Search (mcs)
- Micronaut
- MongoDB
- Mozilla Firefox
- Node.js
- Postman
- Quarkus CLI
- Robo 3T
- sbt
- Scala
- Scala CLI
- ShellCheck
- Spring Boot CLI
- Sublime Merge
- Sublime Text
- Sublime Text 4
- Task
- Tor Browser
- UMLet
- OVH venom
- Visual Studio Code
- VSCodium (Visual Studio Code without Microsoft's telemetry)
- VisualVM
- xq
- xsv
- youtube-dl
- yq
- Zulu OpenJDK

Postman, Adoptium and Visual Studio Code need to be installed with the `-e`
flag if URL completion is used.

### Formerly tested software

Asciinema is no longer tested as it is not delivered anymore as an
[archive](https://discourse.asciinema.org/t/changes-in-asciinema-packaging/90).

Eclipse is no longer tested as it is now delivered as an installer instead of an
archive.

NetBeans is no longer tested as the URL to download a specific version changes
too often for nothing.

## Unit tests

Most `liar` features are tested in
[liar-test](https://framagit.org/grumpyf0x48/liar/-/blob/master/liar-test) file
using [shUnit2](https://github.com/kward/shunit2).

To launch unit tests:

### Install shUnit2

```console
sudo apt-get install shunit2
```

### Create group liar

```console
sudo groupadd liar
```

### Create user liar

```console
sudo adduser --system --ingroup liar --shell /bin/bash liar
```

### Run tests

```console
HOME=/home/liar LIAR=./liar \
    su --preserve-environment --command ./liar-test liar
```

### Run a single test

For example, to run `testHelp`:

```console
HOME=/home/liar LIAR=./liar TEST=testHelp \
    su --preserve-environment --command ./liar-test liar
```

## Docker image

`liar` [docker image](https://hub.docker.com/r/grumpyf0x48/liar) is based on
Debian Bullseye and has the latest development version of liar installed with
Bash command line completion.

To use it:

```console
docker run -it grumpyf0x48/liar
```

## Try it

You can now try `liar` in your browser with
[Repl.it](https://repl.it/@grumpyf0x48/liar):

<a href="https://repl.it/@grumpyf0x48/liar" title="Try Linux Installer for
Archives" target="_blank"><img src="Replit.png" width="50%"
height="50%" /></a>

## Similar software

`liar` provides similar features as the following software:

- [Homebrew](https://docs.brew.sh/Homebrew-on-Linux)
- [SDKMAN](https://sdkman.io)

However, they do not work exactly the same way.

See
[Why not using SDKMAN or Homebrew](#5-why-not-using-sdkman-or-homebrew-instead).

## FAQ

### 1. Why did you write liar

I was upset to:

- Download a zip or tar
- Find the relevant options to extract it
- Decide where to install it
- Create symbolic links to have new binaries in $PATH

### 2. Does liar use a software repository

No.

All `liar` needs to install a software is an archive it can extract. See
[help](#help) for a complete list of supported archive types.

### 3. What is the use of liar-software

The `liar-software` file eases Bash completion of latest (or almost !) URLs for
more than 60 tested software `liar` can install.

It is updated periodically on the Git repository which `liar` polls for updates
at `Bash` startup. You can also manage this file yourself.

### 4. What are the main limitations

Probably the fact that `liar` does not handle software versions.

It is wanted since the beginning as this tool does not use any software
repository, and does not need to have any information about the software it
installs except the URL or path of the archive.

If you need to install multiple versions of the same software, you can do it by
declaring it several times (one per version) in
[`liar-software`](#liar-software) and disable automatic upgrade.

Also updating a software is not handled, yet.

### 5. Why not using SDKMAN or Homebrew instead

You can !

When i was looking for a tool to install software archives, i was looking for
the term **archive** or **tarball** and didn't pay attention to **SDK** one :-
(

### 6. When is scheduled the next release

Most developments to be included in the next version are almost done. So it is
coming !

## License

Copyright (C) 2017 Pierre-Yves Fourmond.

`liar` is free software, distributed under the terms of the
[GNU](https://www.gnu.org) General Public License as published by the
[Free Software Foundation](https://www.fsf.org), version 3 of the License (or
any later version). For more information, see the file [LICENSE](LICENSE.md).

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice and this
notice are preserved. This file is offered as-is, without any warranty.

<a href="https://www.gnu.org/licenses/gpl-3.0.en.html" title="General Public
License" target="_blank"><img
src="https://www.gnu.org/graphics/gplv3-with-text-84x42.png" /></a>
