# Linux Installer for Archives sample use

## Table of contents

- [Installing software](#installing-software)
    + [Installing a JDK in the default destination folder](#installing-a-jdk-in-the-default-destination-folder)
    + [Installing a JDK with symbolic links created](#installing-a-jdk-with-symbolic-links-created)
    + [Installing Sublime Text in another destination folder](#installing-sublime-text-in-another-destination-folder)
    + [Installing Sublime Text from the HTTP URL](#installing-sublime-text-from-the-http-url)
    + [Installing Robomongo with symbolic links created](#installing-robomongo-with-symbolic-links-created)
    + [Installing Maven](#installing-maven)
    + [Installing IntelliJ IDEA in verbose mode with one symbolic link created](#installing-intellij-idea-in-verbose-mode-with-one-symbolic-link-created)
    + [Installing IntelliJ IDEA with checksum verification](#installing-intellij-idea-with-checksum-verification)
    + [Installing Tor Browser](#installing-tor-browser)
    + [Installing Gradle](#installing-gradle)
    + [Installing Go](#installing-go)
    + [Installing GoLand](#installing-goland)
    + [Installing Postman](#installing-postman)
    + [Installing Spring Boot CLI](#installing-spring-boot-cli)
    + [Installing Visual Studio Code](#installing-visual-studio-code)
    * [Installing jd-cmd](#installing-jd-cmd)
    + [Installing a software while being root](#installing-a-software-while-being-root)
- [List what you have installed](#list-what-you-have-installed)
- [Removing previously installed software](#removing-previously-installed-software)

## Installing software

### Installing a JDK in the default destination folder

```console
$ liar install jdk8 https://download.java.net/openjdk/jdk8u40/ri/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz
Installing 'jdk8' from 'https://download.java.net/openjdk/jdk8u40/ri/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' to '/home/user/Programs/jdk8'
2018-07-22 14:29:16 URL:https://download.java.net/openjdk/jdk8u40/ri/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz [174937067/174937067] -> "/home/user/.liar_cache/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz" [1]
Extracting '/home/user/.liar_cache/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' to '/home/user/Programs/jdk8'
```

In this first sample, The JDK has been installed under the name jdk8 in `$HOME/Programs`, which is the default destination folder.

### Installing a JDK with symbolic links created

Here, we specify the relevant pattern so as to create a symbolic link for each binary in `$JAVA_HOME/bin`:

```console
$ liar -v -l -p "bin/*" install jdk_ri8 openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz
Enabling verbose mode
Setting BIN_PATTERNS to 'bin/*'
DESTINATION option is not set, Using '/home/user/Programs' by default
Installing 'jdk_ri8' from 'openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' to '/home/user/Programs/jdk_ri8'
Caching 'openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' to '/home/user/.liar_cache/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz'
Creating '/home/user/Programs/jdk_ri8'
Checking if an installation of '/home/user/.liar_cache/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' already exists in '/home/user/Programs' ...
Extracting '/home/user/.liar_cache/openjdk-8u40-b25-linux-x64-10_feb_2015.tar.gz' to '/home/user/Programs/jdk_ri8'
Creating link: '/home/user/bin/jar' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jar'
Creating link: '/home/user/bin/extcheck' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/extcheck'
Creating link: '/home/user/bin/javac' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/javac'
Creating link: '/home/user/bin/xjc' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/xjc'
Creating link: '/home/user/bin/servertool' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/servertool'
Creating link: '/home/user/bin/jstack' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jstack'
Creating link: '/home/user/bin/idlj' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/idlj'
Creating link: '/home/user/bin/pack200' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/pack200'
Creating link: '/home/user/bin/jconsole' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jconsole'
Creating link: '/home/user/bin/jinfo' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jinfo'
Creating link: '/home/user/bin/jrunscript' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jrunscript'
Creating link: '/home/user/bin/policytool' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/policytool'
Creating link: '/home/user/bin/orbd' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/orbd'
Creating link: '/home/user/bin/serialver' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/serialver'
Creating link: '/home/user/bin/jdb' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jdb'
Creating link: '/home/user/bin/rmiregistry' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/rmiregistry'
Creating link: '/home/user/bin/jps' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jps'
Creating link: '/home/user/bin/javah' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/javah'
Creating link: '/home/user/bin/wsgen' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/wsgen'
Creating link: '/home/user/bin/appletviewer' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/appletviewer'
Creating link: '/home/user/bin/javap' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/javap'
Creating link: '/home/user/bin/rmic' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/rmic'
Creating link: '/home/user/bin/unpack200' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/unpack200'
Creating link: '/home/user/bin/rmid' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/rmid'
Creating link: '/home/user/bin/jdeps' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jdeps'
Creating link: '/home/user/bin/native2ascii' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/native2ascii'
Creating link: '/home/user/bin/jjs' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jjs'
Creating link: '/home/user/bin/jmap' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jmap'
Creating link: '/home/user/bin/jsadebugd' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jsadebugd'
Creating link: '/home/user/bin/java' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/java'
Creating link: '/home/user/bin/jstat' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jstat'
Creating link: '/home/user/bin/java-rmi.cgi' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/java-rmi.cgi'
Creating link: '/home/user/bin/jcmd' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jcmd'
Creating link: '/home/user/bin/keytool' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/keytool'
Creating link: '/home/user/bin/javadoc' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/javadoc'
Creating link: '/home/user/bin/jarsigner' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jarsigner'
Creating link: '/home/user/bin/schemagen' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/schemagen'
Creating link: '/home/user/bin/tnameserv' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/tnameserv'
Creating link: '/home/user/bin/jhat' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jhat'
Creating link: '/home/user/bin/wsimport' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/wsimport'
Creating link: '/home/user/bin/jstatd' -> '/home/user/Programs/jdk_ri8/java-se-8u40-ri/bin/jstatd'
```

### Installing Sublime Text in another destination folder

```console
$ liar -d ~/Extras/ install sublime Downloads/sublime_text_3_build_3126_x64.tar.bz2
Installing 'sublime' from 'Downloads/sublime_text_3_build_3126_x64.tar.bz2' to '/home/user/Extras/sublime'
Extracting '/home/user/.liar_cache/sublime_text_3_build_3126_x64.tar.bz2' to '/home/user/Extras/sublime'
```

Here, Sublime Text has been installed in `$HOME/Extras`, the destination folder specified with option `-d`.

### Installing Sublime Text from the HTTP URL

```console
$ liar install sublime3 http://download.sublimetext.com/sublime_text_3_build_3126_x64.tar.bz2
Installing 'sublime3' from 'http://download.sublimetext.com/sublime_text_3_build_3126_x64.tar.bz2' to '/home/user/Programs/sublime3'
2019-12-28 09:58:11 URL:http://download.sublimetext.com/sublime_text_3_build_3126_x64.tar.bz2 [9313954/9313954] -> "/home/user/.liar_cache/sublime_text_3_build_3126_x64.tar.bz2" [1]
Extracting '/home/user/.liar_cache/sublime_text_3_build_3126_x64.tar.bz2' to '/home/user/Programs/sublime3'
```

### Installing Robomongo with symbolic links created

```console
$ liar -l install robomongo ~/Downloads/robomongo-0.9.0-linux-x86_64-0786489.tar.gz
Installing 'robomongo' from '/home/user/Downloads/robomongo-0.9.0-linux-x86_64-0786489.tar.gz' to '/home/user/Programs/robomongo'
Extracting '/home/user/.liar_cache/robomongo-0.9.0-linux-x86_64-0786489.tar.gz' to '/home/user/Programs/robomongo'
Creating link: '/home/user/bin/robomongo'
```

With this example, Robomongo has been installed with symbolic links created in `~/bin` for all executables found. Here, only one executable has been found. When `liar` removes a software all symbolic links created during installation are removed.

### Installing Maven

```console
$ liar -l install maven3 ~/Downloads/apache-maven-3.3.9-bin.zip
Installing 'maven3' from '/home/user/Downloads/apache-maven-3.3.9-bin.zip' to '/home/user/Programs/maven3'
Extracting '/home/user/.liar_cache/apache-maven-3.3.9-bin.zip' to '/home/user/Programs/maven3'
Creating link: '/home/user/bin/mvn'
Creating link: '/home/user/bin/mvnyjp'
Creating link: '/home/user/bin/mvnDebug'
```

Maven has been installed with symbolic links created in `~/bin` for all executables. In this example, three executables have been found.

To restrict which files are symlinked:

```console
$ liar -l -p mvn install maven3 ~/Downloads/apache-maven-3.3.9-bin.zip
Installing 'maven3' from '/home/user/Downloads/apache-maven-3.3.9-bin.zip' to '/home/user/Programs/maven3'
Extracting '/home/user/Downloads/apache-maven-3.3.9-bin.zip' to '/home/user/Programs/maven3'
Creating link: '/home/user/bin/mvn'
```

Here we have restricted symlink creation to the mvn executable.

### Installing IntelliJ IDEA in verbose mode with one symbolic link created

```console
$ liar -v -l -p idea.sh -h install idea-community https://download.jetbrains.com/idea/ideaIC-2020.2.1.tar.gz
Enabling verbose mode
Enabling links creation
Setting BIN_PATTERNS to 'idea.sh'
Enabling shell extension removal
DESTINATION option is not set, Using '/home/user/Programs' by default
Installing 'idea_community' from 'https://download.jetbrains.com/idea/ideaIC-2020.2.1.tar.gz' to '/home/user/Programs/idea_community'
Downloading 'idea_community' from 'https://download.jetbrains.com/idea/ideaIC-2020.2.1.tar.gz'
Downloading 'https://download.jetbrains.com/idea/ideaIC-2020.2.1.tar.gz' to '/home/user/.local/share/liar/cache/ideaIC-2020.2.1.tar.gz' using 'wget --quiet'
Creating '/home/user/Programs/idea_community'
Checking if an installation of '/home/user/.local/share/liar/cache/ideaIC-2020.2.1.tar.gz' already exists in '/home/user/Programs' ...
Extracting '/home/user/.local/share/liar/cache/ideaIC-2020.2.1.tar.gz' to '/home/user/Programs/idea_community'
Creating link: '/home/user/bin/idea' -> '/home/user/Programs/idea_community/idea-IC-202.6948.69/bin/idea.sh'
```

Here, we have used the `-h` flag to have the link created under the name `idea`.

The other executables available are the ones of the JDK, so we do not create links for them.

### Installing IntelliJ IDEA with checksum verification

```console
$ liar -v -c fa2dfe2697d3e7637cd16f14cec00931d2097c84 install idea ~/Downloads/ideaIC-2017.2.5.tar.gz
Enabling verbose mode
Setting checksum to 'fa2dfe2697d3e7637cd16f14cec00931d2097c84'
DESTINATION option is not set, Using '/home/user/Programs' by default
CHECKSUM type is not set, Using 'sha1' by default
Installing 'idea' from '/home/user/Downloads/ideaIC-2017.2.5.tar.gz' to '/home/user/Programs/idea'
Creating '/home/user/Programs/idea'
Checking if an installation of '/home/user/Downloads/ideaIC-2017.2.5.tar.gz' already exists in '/home/user/Programs' ...
Checking '/home/user/Downloads/ideaIC-2017.2.5.tar.gz' 'sha1' checksum verification
Extracting '/home/user/Downloads/ideaIC-2017.2.5.tar.gz' to '/home/user/Programs/idea'
```

### Installing Tor Browser

```console
$ liar -l -p start-tor-browser install torbrowser ~/Downloads/tor-browser-linux64-6.0.8_fr.tar.xz
Installing 'torbrowser' from '/home/user/Downloads/tor-browser-linux64-6.0.8_fr.tar.xz' to '/home/user/Programs/torbrowser'
Extracting '/home/user/.liar_cache/tor-browser-linux64-6.0.8_fr.tar.xz' to '/home/user/Programs/torbrowser'
Creating link: '/home/user/bin/start-tor-browser'
```

In the Tor Browser installation, we have restricted symbolic link creation to the start-tor-browser executable.

### Installing Gradle

```console
$ liar -v -l -p gradle install gradle ~/Downloads/gradle-4.2.1-bin.zip
Enabling verbose mode
DESTINATION option is not set, Using '/home/user/Programs' by default
Installing 'gradle' from '/home/user/Downloads/gradle-4.2.1-bin.zip' to '/home/user/Programs/gradle'
Caching '/home/user/Downloads/gradle-4.2.1-bin.zip' to '/home/user/.liar_cache/gradle-4.2.1-bin.zip'
Creating '/home/user/Programs/gradle'
Checking if an installation of '/home/user/.liar_cache/gradle-4.2.1-bin.zip' already exists in '/home/user/Programs' ...
Extracting '/home/user/.liar_cache/gradle-4.2.1-bin.zip' to '/home/user/Programs/gradle'
Creating link: '/home/user/bin/gradle' -> '/home/user/Programs/gradle/gradle-4.2.1/bin/gradle'
```

### Installing Go

```console
$ liar -l -p "bin/go" install go https://dl.google.com/go/go1.11.1.linux-amd64.tar.gz
Installing 'go' from 'https://dl.google.com/go/go1.11.1.linux-amd64.tar.gz' to '/home/user/Programs/go'
2018-10-11 04:49:33 URL:https://dl.google.com/go/go1.11.1.linux-amd64.tar.gz [127205934/127205934] -> "/home/user/.liar_cache/go1.11.1.linux-amd64.tar.gz" [1]
Extracting '/home/user/.liar_cache/go1.11.1.linux-amd64.tar.gz' to '/home/user/Programs/go'
Creating link: '/home/user/bin/go'
```

### Installing GoLand

```console
$ liar -v -l -p goland.sh install goland https://download-cf.jetbrains.com/go/goland-2018.2.3.tar.gz
Enabling verbose mode
Setting BIN_PATTERNS to 'goland.sh'
DESTINATION option is not set, Using '/home/user/Programs' by default
Installing 'goland' from 'https://download-cf.jetbrains.com/go/goland-2018.2.3.tar.gz' to '/home/user/Programs/goland'
Downloading 'https://download-cf.jetbrains.com/go/goland-2018.2.3.tar.gz' to '/home/user/.user_cache/goland-2018.2.3.tar.gz' using 'wget --verbose'
--2018-10-11 04:53:15--  https://download-cf.jetbrains.com/go/goland-2018.2.3.tar.gz
Resolving download-cf.jetbrains.com (download-cf.jetbrains.com)... 143.204.192.6, 143.204.192.19, 143.204.192.75, ...
Connecting to download-cf.jetbrains.com (download-cf.jetbrains.com)|143.204.192.6|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: 254186919 (242M) [binary/octet-stream]
Saving to: '/home/user/.user_cache/goland-2018.2.3.tar.gz'

/home/user/.user_cache/goland-201 100%[===========================================================>] 242.41M   582KB/s    in 2m 23s

2018-10-11 04:55:37 (1.70 MB/s) - '/home/user/.user_cache/goland-2018.2.3.tar.gz' saved [254186919/254186919]

Creating '/home/user/Programs/goland'
Checking if an installation of '/home/user/.user_cache/goland-2018.2.3.tar.gz' already exists in '/home/user/Programs' ...
Extracting '/home/user/.user_cache/goland-2018.2.3.tar.gz' to '/home/user/Programs/goland'
Creating link: '/home/user/bin/goland.sh' -> '/home/user/Programs/goland/GoLand-2018.2.3/bin/goland.sh'
```

### Installing Postman

```console
$ liar -e -l -p Postman install postman https://dl.pstmn.io/download/latest/linux64
Enabling content disposition
Installing 'postman' from 'https://dl.pstmn.io/download/latest/linux64' to '/home/user/Programs/postman'
2019-05-04 06:14:28 URL:https://dl.pstmn.io/download/latest/linux64 [68479716/68479716] -> "/home/user/.liar_cache/Postman-linux-x64-7.0.9.tar.gz" [1]
Extracting '/home/user/.liar_cache/Postman-linux-x64-7.0.9.tar.gz' to '/home/user/Programs/postman'
Creating link: '/home/user/bin/Postman'
```

Postman supplied URL makes uses of the content-disposition header, that's why we need to use the `-e` flag here.

### Installing Spring Boot CLI

```console
$ liar -l install spring https://repo.spring.io/release/org/springframework/boot/spring-boot-cli/2.2.2.RELEASE/spring-boot-cli-2.2.2.RELEASE-bin.tar.gz
Installing 'spring' from 'https://repo.spring.io/release/org/springframework/boot/spring-boot-cli/2.2.2.RELEASE/spring-boot-cli-2.2.2.RELEASE-bin.tar.gz' to '/home/user/Programs/spring'
2019-12-28 09:39:28 URL:https://repo.spring.io/release/org/springframework/boot/spring-boot-cli/2.2.2.RELEASE/spring-boot-cli-2.2.2.RELEASE-bin.tar.gz [11369462/11369462] -> "/home/user/.liar_cache/spring-boot-cli-2.2.2.RELEASE-bin.tar.gz" [1]
Extracting '/home/user/.liar_cache/spring-boot-cli-2.2.2.RELEASE-bin.tar.gz' to '/home/user/Programs/spring'
Creating link: '/home/user/bin/spring'
```

### Installing Visual Studio Code

```console
liar -e -l -p code install vscode https://update.code.visualstudio.com/1.49.1/linux-x64/stable
Enabling content disposition
Enabling links creation
Setting BIN_PATTERNS to 'code'
Installing 'vscode' from 'https://update.code.visualstudio.com/1.49.1/linux-x64/stable' to '/home/user/Programs/vscode'
Downloading 'vscode' from 'https://update.code.visualstudio.com/1.49.1/linux-x64/stable'
Extracting '/home/user/.local/share/liar/cache/code-stable-1600299623.tar.gz' to '/home/user/Programs/vscode'
Creating link: '/home/user/bin/code' -> '/home/user/Programs/vscode/VSCode-linux-x64/bin/code'
```

Here we used `-e` option to retrieve the real filename of the latest URL that was given by Bash completion.

A link to `code` binary was also created.

### Installing jd-cmd

```console
$ liar -l install jd_cmd https://github.com/kwart/jd-cmd/releases/download/jd-cmd-1.1.0.Final/jd-cli-1.1.0.Final-dist.zip
Enabling links creation
Installing 'jd_cmd' from 'https://github.com/kwart/jd-cmd/releases/download/jd-cmd-1.1.0.Final/jd-cli-1.1.0.Final-dist.zip' to '/home/user/Programs/jd_cmd'
Downloading 'jd_cmd' from 'https://github.com/kwart/jd-cmd/releases/download/jd-cmd-1.1.0.Final/jd-cli-1.1.0.Final-dist.zip'
Extracting '/home/user/.local/share/liar/cache/jd-cli-1.1.0.Final-dist.zip' to '/home/user/Programs/jd_cmd'
Creating link: '/home/user/bin/jd-cli' -> '/home/user/Programs/jd_cmd/jd-cli'
```

### Installing a software while being root

```console
$ sudo liar install maven ~/Downloads/apache-maven-3.3.9-bin.tar.gz
liar cannot be run as root. Please use '-r' option.
```

By default `liar` does not allow root installation for security reasons.

Then when adding `-r` option:

```console
$ sudo liar -r install maven ~/Downloads/apache-maven-3.3.9-bin.tar.gz
Allowing root usage
Installing 'maven' from '/home/user/Downloads/apache-maven-3.3.9-bin.tar.gz' to '/root/Programs/maven'
Extracting '/root/.liar_cache/apache-maven-3.3.9-bin.tar.gz' to '/root/Programs/maven'
```

## List what you have installed

```console
$ liar list
Listing installed software in '/home/user/Programs'

Name           Version    Folder             File Name                              Date

visualvm2      142        visualvm_142       visualvm_142.zip                       2020/04/11
spring         2.2.2      spring-2.2.2.REL   spring-boot-cli-2.2.2.RELEASE-bin.zi   2019/12/28
maven          3.6.3      apache-maven-3.6   apache-maven-3.6.3-bin.tar.gz          2019/12/28
torbrowser     7.5.4      tor-browser_fr     tor-browser-linux64-7.5.4_fr.tar.xz    2018/05/26
```

list command displays installation name, version, folder, from which archive file the installation was done and the installation date.

## Removing previously installed software

```console
$ liar remove sublime3
Removing 'sublime3' installed in '/home/user/Programs/sublime3'
Do you want to remove 'sublime3' installed in '/home/user/Programs/sublime3' (Y/N) ? Y
```

When removing a software, a confirmation is asked to the user unless `-f` option is used.

Moreover, this command can also remove multiple software at once, for example:

```console
$ liar remove sublime3 robo3t
```
